﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plantas_Rodrigo_1600176.Classes;

namespace Plantas_Rodrigo_1600176
{
    public class Program
    {
        static void Main(string[] args)
        {
			Ornamentais o = new Ornamentais();
			o.Caracteristica = "fica no alto";
			o.Nome = "flor branca";
			o.ID = 1;

			
			Console.WriteLine("Planta: " + o.ObterNome());


			foreach (var item in o.GerarLista())
			{
				Console.WriteLine();
			}

			Console.ReadKey();
		}
    }
}
